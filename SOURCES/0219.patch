From 3aca4acaea9569573d3d0e83c3c7998b56ea74c4 Mon Sep 17 00:00:00 2001
From: Hari Bathini <hbathini@linux.ibm.com>
Date: Tue, 12 Jul 2022 13:55:47 +0530
Subject: [PATCH] fix(squash): build ld cache for squash loader

Commit dc21638c3f0a fixes kdump kernel crash, due to non-conventional
library path in powerpc, by copying /etc/ld.so.cache under initdir.
While that works in general, it is vulnerable to failure because of
missing links, when the CPU is reconfigured to run in compatibility
mode of older CPU version. Avoid this by running ldconfig for squash
loader to create the necessary links & ld cache for it. Doing this
also saves a few kilobyes of space as the copied ld cache is bigger
in size than the one needed for squash loader environment.

Fixes: dc21638c3f0a ("fix(squash): keep ld cache under initdir")
Cc: Pingfan Liu <piliu@redhat.com>
Signed-off-by: Hari Bathini <hbathini@linux.ibm.com>

(Cherry-picked from f5e8250a06b5a53f6fd8e47ec976db933c91b3cd)

Resolves: #2055026
---
 dracut-init.sh                     | 14 ++++++++++++++
 dracut.sh                          | 11 +----------
 modules.d/99squash/module-setup.sh |  8 +++-----
 3 files changed, 18 insertions(+), 15 deletions(-)

diff --git a/dracut-init.sh b/dracut-init.sh
index 032c38c2..5df2e583 100644
--- a/dracut-init.sh
+++ b/dracut-init.sh
@@ -443,6 +443,20 @@ inst_rules_wildcard() {
     [[ $_found ]] || dinfo "Skipping udev rule: $_rule"
 }
 
+# make sure that library links are correct and up to date
+build_ld_cache() {
+    for f in "$dracutsysrootdir"/etc/ld.so.conf "$dracutsysrootdir"/etc/ld.so.conf.d/*; do
+        [[ -f $f ]] && inst_simple "${f#$dracutsysrootdir}"
+    done
+    if ! $DRACUT_LDCONFIG -r "$initdir" -f /etc/ld.so.conf; then
+        if [[ $EUID == 0 ]]; then
+            derror "ldconfig exited ungracefully"
+        else
+            derror "ldconfig might need uid=0 (root) for chroot()"
+        fi
+    fi
+}
+
 prepare_udev_rules() {
     [ -z "$UDEVVERSION" ] && export UDEVVERSION=$(udevadm --version | { read v _ ; echo $v ; })
 
diff --git a/dracut.sh b/dracut.sh
index 702b2f78..57f51e91 100755
--- a/dracut.sh
+++ b/dracut.sh
@@ -1601,16 +1601,7 @@ done
 
 if [[ $kernel_only != yes ]]; then
     # make sure that library links are correct and up to date
-    for f in /etc/ld.so.conf /etc/ld.so.conf.d/*; do
-        [[ -f $f ]] && inst_simple "$f"
-    done
-    if ! ldconfig -r "$initdir"; then
-        if [[ $EUID = 0 ]]; then
-            derror "ldconfig exited ungracefully"
-        else
-            derror "ldconfig might need uid=0 (root) for chroot()"
-        fi
-    fi
+    build_ld_cache
 fi
 
 if [[ $do_hardlink = yes ]] && command -v hardlink >/dev/null; then
diff --git a/modules.d/99squash/module-setup.sh b/modules.d/99squash/module-setup.sh
index e73d3184..8a0923fb 100644
--- a/modules.d/99squash/module-setup.sh
+++ b/modules.d/99squash/module-setup.sh
@@ -28,11 +28,6 @@ installpost() {
         [[ "$squash_dir" == "$i"/* ]] || mv "$i" "$squash_dir"/
     done
 
-    # initdir also needs ld.so.* to make ld.so work
-    inst /etc/ld.so.cache
-    inst /etc/ld.so.conf
-    inst_dir /etc/ld.so.conf.d
-
     # Create mount points for squash loader
     mkdir -p "$initdir"/squash/
     mkdir -p "$squash_dir"/squash/
@@ -68,6 +63,9 @@ installpost() {
     ln_r /usr/bin /bin
     ln_r /usr/sbin /sbin
     inst_simple "$moddir"/init-squash.sh /init
+
+    # make sure that library links are correct and up to date for squash loader
+    build_ld_cache
 }
 
 install() {

